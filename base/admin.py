from django.contrib import admin

from .models import *
# Register your models here.

admin.site.register(Category)
admin.site.register(Menu)
admin.site.register(Table)

admin.site.register(Order_delivery)
admin.site.register(Order_restaurant)


admin.site.register(Reservation)
admin.site.register(Feedback)
admin.site.register(Contact_detail)
admin.site.register(Call_waiter)
