from email.policy import default
from django.db import models
import uuid

from django.forms import CharField

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=200, null=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    date_updated = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        ordering = ['-date_updated', '-date_created']

    def __str__(self):
        return self.name


class Menu(models.Model):
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    price = models.FloatField(max_length=200, null=True, blank=True)
    image = models.ImageField(upload_to='images/', null=True, blank=True)
    description = models.TextField(max_length=200, null=True, blank=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_updated = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        ordering = ['-date_updated', '-date_created']

    def __str__(self):
        return self.name


class Table(models.Model):
    table_Number = models.IntegerField(null=True)
    size = models.IntegerField(null=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_updated = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        ordering = ['-date_updated', '-date_created']

    def __str__(self):
        return "Table - %s " % (self.table_Number)



class Order_restaurant(models.Model):
    table = models.ForeignKey(
        Table, on_delete=models.CASCADE, null=True, blank=True)
    menu = models.ForeignKey(
        Menu, on_delete=models.CASCADE, null=True, blank=True)
    quantity = models.IntegerField(null=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_updated = models.DateTimeField(auto_now=True, null=True)

    @property
    def get_total(self):
        total = self.menu.price * self.quantity
        return total


    class Meta:
        ordering = ['-date_updated', '-date_created']


class Order_delivery(models.Model):
    address = models.CharField(max_length=200, null=True)
    customer_name = models.CharField(max_length=200, null=True)
    menu = models.ForeignKey(
        Menu, on_delete=models.CASCADE, null=True, blank=True)
    quantity = models.IntegerField(null=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_updated = models.DateTimeField(auto_now=True, null=True)

    @property
    def get_total(self):
        total = self.menu.price * self.quantity
        return total

    class Meta:
        ordering = ['-date_updated', '-date_created']


class Call_waiter(models.Model):
    table = models.ForeignKey(
        Table, on_delete=models.CASCADE, null=True, blank=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_updated = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        ordering = ['-date_updated', '-date_created']


class Reservation(models.Model):
    table = models.ForeignKey(
        Table, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=200, null=True)
    email = models.EmailField(max_length=100, null=True)
    phone = models.CharField(max_length=200, null=True)
    note = models.CharField(max_length=100, null=True)
    date_visit = models.DateTimeField(null=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    date_updated = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        ordering = ['-date_updated', '-date_created']

    def __str__(self):
        return self.name


class Feedback(models.Model):
    CLASSIFY = (
        ('Poor', 'Poor'),
        ('Good', 'Good'),
        ('Excellent', 'Excellent'),
    )
    ambiance = models.CharField(max_length=100, null=True, choices=CLASSIFY)
    food = models.CharField(max_length=100, null=True, choices=CLASSIFY)
    service = models.CharField(max_length=100, null=True, choices=CLASSIFY)
    customer_name = models.CharField(max_length=100, null=True)
    email = models.EmailField(max_length=100, null=True)
    phone = models.CharField(max_length=200, null=True)
    suggestions = models.CharField(max_length=200, null=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    date_updated = models.DateTimeField(auto_now=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        ordering = ['-date_updated', '-date_created']


class Contact_detail(models.Model):
    restaurant_name = models.CharField(max_length=200, null=True)
    email = models.EmailField(max_length=100, null=True)
    phone = models.CharField(max_length=200, null=True)
    address = models.CharField(max_length=200, null=True)
    city = models.CharField(max_length=200, null=True)
    state = models.CharField(max_length=200, null=True)
    id = models.UUIDField(default=uuid.uuid4, unique=True,
                          primary_key=True, editable=False)
    date_updated = models.DateTimeField(auto_now=True, null=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True)

    class Meta:
        ordering = ['-date_updated', '-date_created']