from django.urls import path, re_path
from . import views


urlpatterns = [
    path('', views.home, name="home"),
    path('login/', views.loginPage, name="login"),
    path('logout/', views.logoutPage, name="logout"),
    path('registration/', views.registrationPage, name="registration"),
    path('profile/', views.profile, name="profile"),

    #Order in Restaurant Management
    path('order_restaurant/', views.order_restaurant, name="order_restaurant"),
    path('order_restaurant_create/', views.createOrderRestaurant, name="order_restaurant_create"),
    path('order_restaurant_update/<uuid:pk>/', views.updateOrderRestaurant, name="order_restaurant_update"),
    path('order_restaurant_delete/<uuid:pk>/', views.deleteOrderRestaurant, name="order_restaurant_delete"),
    path('show_order_restaurant/<uuid:pk>/', views.ShowOrderResaturant, name="show_order_restaurant"),

    #Order Delivery Management
    path('order_delivery/', views.order_delivery, name="order_delivery"),
    path('order_delivery_create/', views.createOrderDelivery, name="order_delivery_create"),
    path('order_delivery_update/<uuid:pk>/', views.updateOrderDelivery, name="order_delivery_update"),
    path('order_delivery_delete/<uuid:pk>/', views.deleteOrderDelivery, name="order_delivery_delete"),



    #Reservations Management
    path('reservation/', views.reservation, name="reservation"),
    path('reservation_create/', views.createReservation, name="reservation_create"),
    path('reservation_update/<uuid:pk>/', views.updateReservation, name="reservation_update"),
    path('reservation_delete/<uuid:pk>/', views.deleteReservation, name="reservation_delete"),
    

    #Tables Management
    path('table/', views.table, name="table"),
    path('table_create/', views.createTable, name="table_create"),
    path('table_update/<uuid:pk>/', views.updateTable, name="table_update"),
    path('table_delete/<uuid:pk>/', views.deleteTable, name="table_delete"),


    #Categories Management
    path('category/', views.category, name="category"),
    path('category_create/', views.createCategory, name="category_create"),
    path('category_update/<uuid:pk>/', views.updateCategory, name="category_update"),
    path('category_delete/<uuid:pk>/', views.deleteCategory, name="category_delete"),

    #Menu Management
    path('menu/', views.menu, name="menu"),
    path('menu_create/', views.createMenu, name="menu_create"),
    path('menu_update/<uuid:pk>/', views.updateMenu, name="menu_update"),
    path('menu_delete/<uuid:pk>/', views.deleteMenu, name="menu_delete"),

    path('feedback/', views.feedback, name="feedback"),
    path('feedback_delete/<uuid:pk>/', views.deleteFeedback, name="feedback_delete"),
    path('call_waiter/', views.call_waiter, name="call_waiter"),
    path('qr_share/', views.qr_share, name="qr_share"),
    path('staff/', views.staff, name="staff"),
    path('staff_create/', views.createStaff, name="staff_create"),
    


]