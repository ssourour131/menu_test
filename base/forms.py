from tokenize import group
from django import forms
from django.forms import ModelForm, modelform_factory, widgets
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from .models import *


class CreateUserForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["username"].widget.attrs.update({
            'required': '',
            'name': 'username',
            'id': 'username',
            'type': 'text',
            'class': 'form-control',
            'placeholder': 'type your username',
            'maxlength': '16',
            'minlength': '6',
        })
        self.fields["email"].widget.attrs.update({
            'required': '',
            'name': 'email',
            'id': 'email',
            'type': 'email',
            'class': 'form-control',
            'placeholder': 'type your email',
        })

        self.fields["password1"].widget.attrs.update({
            'required': '',
            'name': 'password1',
            'id': 'password1',
            'type': 'password',
            'class': 'form-control',
            'placeholder': 'type your password',
            'maxlength': '22',
            'minlength': '8',
        })
        self.fields["password2"].widget.attrs.update({
            'required': '',
            'name': 'password1',
            'id': 'password1',
            'type': 'password',
            'class': 'form-control',
            'placeholder': 'type your password',
            'maxlength': '22',
            'minlength': '8',
        })
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class TableForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["table_Number"].widget.attrs.update({
            'required': '',
            'name': 'table_Number',
            'id': 'table_Number',
            'type': 'number',
            'class': 'form-control',
            'placeholder': 'enter a number',

        })
        self.fields["size"].widget.attrs.update({
            'required': '',
            'name': 'size',
            'id': 'size',
            'type': 'number',
            'class': 'form-control',
            'placeholder': 'enter a size',
        })

    class Meta:
        model = Table
        fields = '__all__'


class CategoryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["name"].widget.attrs.update({
            'required': '',
            'name': 'name',
            'class': 'form-control',
        })
    class Meta:
        model = Category
        fields = '__all__'


class MenuForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["category"].widget.attrs.update({
            'required': '',
            'name': 'category',
            'class': 'form-control',
        })
        self.fields["name"].widget.attrs.update({
            'required': '',
            'name': 'name',
            'type': 'text',
            'class': 'form-control',
        })

        self.fields["price"].widget.attrs.update({
            'required': '',
            'name': 'price',
            'type': 'number',
            'class': 'form-control',

        })
        self.fields["image"].widget.attrs.update({
            'required': '',
            'name': 'image',

            'type': 'image',
            'class': 'form-control',

        })
        self.fields["description"].widget.attrs.update({
            'required': '',
            'name': 'description',
            'type': 'text',
            'class': 'form-control',
        })
    class Meta:
        model = Menu
        fields = '__all__'


class ReservationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["table"].widget.attrs.update({
            'required': '',
            'name': 'table',
            'id': 'sel1',
            'class': 'form-control',
        })
        self.fields["name"].widget.attrs.update({
            'required': '',
            'name': 'name',
            'class': 'form-control',
            'type': 'text',

        })
        self.fields["phone"].widget.attrs.update({
            'required': '',
            'name': 'number',
            'type': 'number',
            'class': 'form-control',

        })
        self.fields["email"].widget.attrs.update({
            'required': '',
            'name': 'email',
            'id': 'email',
            'type': 'email',
            'class': 'form-control',

        })
        self.fields["note"].widget.attrs.update({

            'name': 'note',
            'class': 'form-control',
            'type': 'text',

        })
        self.fields["date_visit"].widget.attrs.update({
            'required': '',
            'name': 'date_visit',
            'class': 'form-control',
            'type': 'date',

        })
    class Meta:
        model = Reservation
        fields = '__all__'

class OrderDeliveryForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["address"].widget.attrs.update({
            'required': '',
            'name': 'address',
            'class': 'form-control',
        })
        self.fields["customer_name"].widget.attrs.update({
            'required': '',
            'name': 'customer_name',
            'class': 'form-control',
        })
        self.fields["menu"].widget.attrs.update({
            'required': '',
            'name': 'menu',
            'class': 'form-control',
        })
        self.fields["quantity"].widget.attrs.update({
            'required': '',
            'name': 'quantity',
            'type': 'number',
            'class': 'form-control',
        })
    class Meta:
        model = Order_delivery
        fields = '__all__'

class OrderRestaurantForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["table"].widget.attrs.update({
            'required': '',
            'name': 'table',
            'id': 'sel1',
            'class': 'form-control',
        })
        self.fields["menu"].widget.attrs.update({
            'required': '',
            'name': 'menu',
            'class': 'form-control',
        })
        self.fields["quantity"].widget.attrs.update({
            'required': '',
            'name': 'quantity',
            'type': 'number',
            'class': 'form-control',
        })
    class Meta:
        model = Order_restaurant
        fields = '__all__'


class StaffForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["username"].widget.attrs.update({
            'required': '',
            'name': 'username',
            'id': 'username',
            'type': 'text',
            'class': 'form-control',

            'maxlength': '16',
            'minlength': '6',
        })
        self.fields["email"].widget.attrs.update({
            'required': '',
            'name': 'email',
            'id': 'email',
            'type': 'email',
            'class': 'form-control',
        })

        self.fields["password1"].widget.attrs.update({
            'required': '',
            'name': 'password1',
            'id': 'password1',
            'type': 'password',
            'class': 'form-control',
            'maxlength': '22',
            'minlength': '8',
        })
        self.fields["password2"].widget.attrs.update({
            'required': '',
            'name': 'password1',
            'id': 'password1',
            'type': 'password',
            'class': 'form-control',
            'maxlength': '22',
            'minlength': '8',
        })
    
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']