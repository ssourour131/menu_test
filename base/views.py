from multiprocessing import context
from django.contrib.auth.models import Group
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm
from django.forms import inlineformset_factory
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .models import *
from .forms import *

from .decorators import unauthenticated_user, allowed_users
# Create your views here.

@unauthenticated_user
def registrationPage (request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user  = form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + user)

            return redirect('login')

    context = {'form': form}
    return render(request, 'base/registration.html', context)
        
@unauthenticated_user
def loginPage (request):
    
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password )

        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.error(request,'Username OR password is incorrect')

    return render(request, 'base/login.html')


def logoutPage(request):
    logout(request)
    return redirect('login')

@login_required(login_url='login')
def home(request):
    return render(request, 'base/home.html')

@login_required(login_url='login')
def profile(request):
    return render(request, 'base/profile.html')

@login_required(login_url='login')
def category(request):
    categorys = Category.objects.all()
    context = {'categorys': categorys}
    return render(request, 'base/category.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def createCategory(request):
    form = CategoryForm()
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your category was created successfully!', extra_tags='alert')
            return redirect('category')

    context = {'form': form}
    return render(request, 'base/forms/category_form.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def updateCategory(request, pk):
    category = Category.objects.get(id=pk)
    form = CategoryForm(instance=category)
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = CategoryForm(request.POST, instance=category)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your category was updated successfully!', extra_tags='alert')
            return redirect('category')

    context = {'form': form}
    return render(request, 'base/forms/category_form.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def deleteCategory(request, pk):
    category = Category.objects.get(id=pk)
    if request.method == 'POST':
        category.delete()
        messages.success(
            request, 'Your category was deleted successfully!', extra_tags='alert')
        return redirect('category')

    context = {'item': category}
    return render(request, 'base/forms/delete_category.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def menu(request):
    menus = Menu.objects.all()
    categorys = Category.objects.all()
    context = {'menus': menus, 'categorys': categorys}
    return render(request, 'base/menu.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def createMenu(request):
    form = MenuForm()
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = MenuForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your menu item was created successfully!', extra_tags='alert')
            return redirect('menu')

    context = {'form': form}
    return render(request, 'base/forms/menu_form.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def updateMenu(request, pk):
    menu = Menu.objects.get(id=pk)
    form = MenuForm(instance=menu)
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = MenuForm(request.POST, request.FILES, instance=menu)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your menu item was updated successfully!', extra_tags='alert')
            return redirect('menu')

    context = {'form': form}
    return render(request, 'base/forms/menu_form.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def deleteMenu(request, pk):
    menu = Menu.objects.get(id=pk)
    if request.method == 'POST':
        menu.delete()
        messages.success(
            request, 'Your menu item was deleted successfully!', extra_tags='alert')
        return redirect('menu')

    context = {'item': menu}
    return render(request, 'base/forms/delete_menu.html', context)

@login_required(login_url='login')
def order_restaurant(request):
    order_restaurants = Order_restaurant.objects.all()
    context = {'order_restaurants': order_restaurants}
    return render(request, 'base/order_restaurant.html', context)

@login_required(login_url='login')
def ShowOrderResaturant(request,pk):
    orders = Order_restaurant.objects.filter(id=pk)
    context = {'orders':orders }
    return render(request, 'base/show_order_restaurant.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def createOrderRestaurant(request):
    form = OrderRestaurantForm()
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = OrderRestaurantForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your Order was created successfully!', extra_tags='alert')
            return redirect('order_restaurant')

    context = {'form': form}
    return render(request, 'base/forms/order_restaurant_form.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def updateOrderRestaurant(request, pk):
    order = Order_restaurant.objects.get(id=pk)
    form = OrderRestaurantForm(instance=order)
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = OrderRestaurantForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your Order was updated successfully!', extra_tags='alert')
            return redirect('order_restaurant')

    context = {'form': form}
    return render(request, 'base/forms/order_restaurant_form.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def deleteOrderRestaurant(request, pk):
    order = Order_restaurant.objects.get(id=pk)
    if request.method == 'POST':
        order.delete()
        messages.success(
            request, 'Your order was deleted successfully!', extra_tags='alert')
        return redirect('order_restaurant')

    context = {'item': order}
    return render(request, 'base/forms/delete_order_restautant.html', context)


@login_required(login_url='login')
def order_delivery(request):
    order_deliverys = Order_delivery.objects.all()
    context = {'order_deliverys': order_deliverys}
    return render(request, 'base/order_delivery.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def createOrderDelivery(request):
    form = OrderDeliveryForm()
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = OrderDeliveryForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your Order was created successfully!', extra_tags='alert')
            return redirect('order_delivery')

    context = {'form': form}
    return render(request, 'base/forms/order_delivery_form.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def updateOrderDelivery(request, pk):
    order = Order_delivery.objects.get(id=pk)
    form = OrderDeliveryForm(instance=order)
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = OrderDeliveryForm(request.POST, instance=order)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your Order was created successfully!', extra_tags='alert')
            return redirect('order_delivery')

    context = {'form': form}
    return render(request, 'base/forms/order_delivery_form.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def deleteOrderDelivery(request, pk):
    order = Order_delivery.objects.get(id=pk)
    if request.method == 'POST':
        order.delete()
        messages.success(
            request, 'Your table was deleted successfully!', extra_tags='alert')
        return redirect('order_delivery')

    context = {'item': order}
    return render(request, 'base/forms/delete_order_delivery.html', context)


@login_required(login_url='login')
def table(request):
    tables = Table.objects.all()
    context = {'tables': tables}
    return render(request, 'base/table.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def createTable(request):
    form = TableForm()
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = TableForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your table was created successfully!', extra_tags='alert')
            return redirect('table')

    context = {'form': form}
    return render(request, 'base/forms/table_form.html', context)

@login_required(login_url='login')
def createOrderTable(request,pk):
    OrderFormSet = inlineformset_factory(Table, Order_restaurant, fields=('menu','quantity'),extra=2)

    table = Table.objects.get(id=pk)
    formset = OrderFormSet(instance=table)
    # form = OrderRestaurantForm(initial={'table':table})
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        formset = OrderRestaurantForm(request.POST,instance=table)
        if formset.is_valid():
            formset.save()
            messages.success(
                request, 'Your Order was created successfully!', extra_tags='alert')
            return redirect('table')

    context = {'formset': formset}
    return render(request, 'base/forms/order_from_table.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def updateTable(request, pk):
    table = Table.objects.get(id=pk)
    form = TableForm(instance=table)
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = TableForm(request.POST, instance=table)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your table was updated successfully!', extra_tags='alert')
            return redirect('table')

    context = {'form': form}
    return render(request, 'base/forms/table_form.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def deleteTable(request, pk):
    table = Table.objects.get(id=pk)
    if request.method == 'POST':
        table.delete()
        messages.success(
            request, 'Your table was deleted successfully!', extra_tags='alert')
        return redirect('table')

    context = {'item': table}
    return render(request, 'base/forms/delete_table.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def waitstaff(request):
    waiterstaffs = User.objects.all()
    context = {'waiterstaffs': waiterstaffs}
    return render(request, 'base/waitstaff.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def feedback(request):
    feedbacks = Feedback.objects.all()
    context = {'feedbacks': feedbacks}
    return render(request, 'base/feedback.html', context)

@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def deleteFeedback(request, pk):
    feedback = Feedback.objects.get(id=pk)
    if request.method == 'POST':
        feedback.delete()
        messages.success(
            request, 'Your table was deleted successfully!', extra_tags='alert')
        return redirect('feedback')

    context = {'item': feedback}
    return render(request, 'base/forms/delete_feedback.html', context)

@login_required(login_url='login')
def call_waiter(request):
    calls = Call_waiter.objects.all()
    context = {'calls': calls}
    return render(request, 'base/call_waiter.html', context)

@login_required(login_url='login')
def reservation(request):
    reservations = Reservation.objects.all()
    context = {'reservations': reservations}
    return render(request, 'base/reservation.html', context)

@login_required(login_url='login')
def createReservation(request):
    form = ReservationForm()
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = ReservationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your reservation was created successfully!', extra_tags='alert')
            return redirect('reservation')

    context = {'form': form}
    return render(request, 'base/forms/reservation_form.html', context)

@login_required(login_url='login')
def updateReservation(request,pk):
    reservation = Reservation.objects.get(id=pk)
    form = ReservationForm(instance=reservation)
    if request.method == 'POST':
        # print('Pring POST', request.POST)
        form = ReservationForm(request.POST,instance=reservation)
        if form.is_valid():
            form.save()
            messages.success(
                request, 'Your reservation was updated successfully!', extra_tags='alert')
            return redirect('reservation')

    context = {'form': form}
    return render(request, 'base/forms/reservation_form.html', context)

@login_required(login_url='login')
def deleteReservation(request, pk):
    reservation = Reservation.objects.get(id=pk)
    if request.method == 'POST':
        reservation.delete()
        messages.success(
            request, 'Your reservation was deleted successfully!', extra_tags='alert')
        return redirect('reservation')

    context = {'item': reservation}
    return render(request, 'base/forms/delete_reservation.html', context)


@login_required(login_url='login')
@allowed_users(allowed_roles=['manager'])
def qr_share(request):
    return render(request, 'base/qr_share.html')

def staff(request):
    staffs = User.objects.all()
    context = {'staffs': staffs}
    return render(request,'base/staff.html',context)

def createStaff(request):
    form = StaffForm()
    if request.method == 'POST':
        form = StaffForm(request.POST)
        if form.is_valid():
            form.save()
            user  = form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + user)

            return redirect('staff')

    context = {'form': form}
    return render(request,'base/forms/staff_form.html',context)
    