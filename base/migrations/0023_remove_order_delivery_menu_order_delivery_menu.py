# Generated by Django 4.0.3 on 2022-03-20 09:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0022_remove_order_delivery_menu_order_delivery_menu'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order_delivery',
            name='menu',
        ),
        migrations.AddField(
            model_name='order_delivery',
            name='menu',
            field=models.ManyToManyField(to='base.menu'),
        ),
    ]
