# Generated by Django 4.0.3 on 2022-03-16 12:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0004_call_waiter_contact_detail_feedback_menu_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='menu',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='base.category'),
        ),
    ]
